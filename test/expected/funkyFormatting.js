define(
    'test/input/funkyFormatting',
    ["f/o/o", "not/related"], function(foo, NotRelated) {
        NotRelated.init();
        foo.run("a random command");

        return foo.results[1];
    }
);
