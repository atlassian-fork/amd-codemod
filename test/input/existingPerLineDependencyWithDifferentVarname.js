define("test/input/existingPerLineDependencyWithDifferentVarname", ["require"], function(require) {
    var somedep = require("some/dep");
    var whatthefoo = require("what/the/foo");
    //  ^^^^^^^^^^ var should still be named 'whatthefoo'.
    var someotherdep = require("some/other/dep");
    whatthefoo.setOptions({ // <-- var should still be named 'whatthefoo'
        something: true
    });
});
