function ImportUtil(jscodeshift) {
    this.j = jscodeshift;
}

ImportUtil.prototype.createImportStatement = function(moduleName, variableName, propName, comments) {
    var j = this.j;
    var declaration, variable, idIdentifier, nameIdentifier;
    // console.log('variableName', variableName);
    // console.log('moduleName', moduleName);

    // if no variable name, return `import 'jquery'`
    if (!variableName) {
        declaration = j.importDeclaration([], j.literal(moduleName) );
        declaration.comments = comments
        return declaration;
    }

    // multiple variable names indicates a destructured import
    if (Array.isArray(variableName)) {
        var variableIds = variableName.map(function(v) {
            return j.importSpecifier(j.identifier(v), j.identifier(v));
        });

        declaration = j.importDeclaration(variableIds, j.literal(moduleName));
    } else {
        // else returns `import $ from 'jquery'`
        nameIdentifier = j.identifier(variableName); //import var name
        variable = j.importDefaultSpecifier(nameIdentifier);

        // if propName, use destructuring `import {pluck} from 'underscore'`
        if (propName && propName !== 'default') {
            idIdentifier = j.identifier(propName);
            variable = j.importSpecifier(idIdentifier, nameIdentifier); // if both are same, one is dropped...
        }

        declaration = j.importDeclaration([variable], j.literal(moduleName) );
    }
    declaration.comments = comments;
    return declaration;
}

module.exports = ImportUtil;
